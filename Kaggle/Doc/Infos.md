# FAA 2018: Cognate Identification


## Vamour Valentin
## Pruvost Julien

````
https://www.kaggle.com/c/faa2018-cognates

Fichier à rendre : 2 colonnes : Id, catégorie


Pistes:
-Distance entre les mots
-Longueur des mots
-Calculer les relations entre les langues (romain/asiat => 0 relation)
````


## Etape 1 : Représentation des données
#### Définir et retravailler les données.

#### Définir la "distance entre 2 langues".
https://fr.wikipedia.org/wiki/Famille_de_langues<br/>
https://fr.wikipedia.org/wiki/Langues_par_famille<br/>
http://udel.edu/~dlarsen/ling203/Slides/Language%20Families.pdf<br/>
https://linguistlist.org/indexfd.cfm<br/>

####  Distance entre 2 mots.
https://fr.wikipedia.org/wiki/Distance_de_Levenshtein<br/>
http://lingpy.org/docu/index.html<br/>


### Etape 2 : Choisir un algorithme
> Perceptron
> Ridge

> Retransformer les données (les élever à une puissance x)