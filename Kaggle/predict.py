
# coding: utf-8

# In[1]:


# Import des librairies de base
import numpy as np
import pandas as pd
import difflib
import matplotlib.pyplot as plt
import tqdm
from math import sqrt 
# On importe les modules de phonétiques
from pyphonetics import Metaphone
from pyphonetics import Soundex
from pyphonetics import RefinedSoundex

# Import sklearn
from sklearn.linear_model import Perceptron
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import RidgeClassifier
from sklearn.model_selection import KFold
from sklearn.metrics import r2_score, mean_squared_error


languages = pd.read_csv("./languoid.csv",sep =";")


# In[ ]:


meta = Metaphone()
soun = Soundex();
rs = RefinedSoundex()
meta_dict = dict()
soun_dict = dict()
def phonetic_score(w1, w2):    
    """
        Permet de calculer les scores phonétiques de deux mots
        
        :param w1: Le premier mot
        :type w1: str
        :param w2: Le second mot
        :type w2: str
        :return: Une liste des scores phonétiques
        :rtype: list
        
        :Example:
        >>> phonetic_score("Robert", "Rupert")
        [0, 0]
        >>> phonetic_score("Albert", "Rupert")
        [3,2]
    """    
        
    try:
        if w1 in meta_dict:
            metaw1 =  meta_dict[w1]
        else:
            meta_dict[w1] = meta.phonetics(w1)
            metaw1 = meta_dict[w1]
        if w2 in meta_dict:
            metaw2 =  meta_dict[w2]
        else:
            meta_dict[w2] = meta.phonetics(w2)
            metaw2 = meta_dict[w2]
        
        if w1 in soun_dict:
            sounw1 =  soun_dict[w1]
        else:
            soun_dict[w1] = soun.phonetics(w1)
            sounw1 = soun_dict[w1]
        if w2 in soun_dict:
            sounw2 =  soun_dict[w2]
        else:
            soun_dict[w2] = soun.phonetics(w2)        
            sounw2 = soun_dict[w2]
        
        if(metaw1 != '' and metaw2 != ''):
            meta_score   = rs.distance(metaw1, metaw2, metric='levenshtein')
        else:
            meta_score   = 0
            
        soun_score   = rs.distance(sounw1, sounw2, metric='levenshtein')
        nysiis_score = rs.distance(w1, w2, metric='levenshtein')
    except IndexError:
        return [-1, -1, -1]
        
    return [meta_score, soun_score, nysiis_score]

criterias = ['meta_score','soun_score','nysiis_score','word size','prefix','macroarea','genus','family','geo dist']
def load_data():    
    print("ENTRAINEMENT")
    print("Récupération des données d'entrainement")
    train = pd.read_csv("./IE2012-lexical-data.train.csv")
    
    y_train_list = train["Category"]
    
    X_train_list = list()
    
    print("Transformation des données")
    for i in tqdm.tqdm(range(len(train["word1"]))):
        w1 = str(train["word1"][i])
        w2 = str(train["word2"][i])
        l1 = find_language(train["language1"][i])
        l2 = find_language(train["language2"][i])
        
        #Critères phonetic
        if(type(w1) == str and type(w2) == str and w1 != '' and w2 != ''):
            criterias = phonetic_score(w1, w2)
            #Taille des mots
            criterias.append(abs(len(w1)-len(w2)))
            #Prefix
            s = difflib.SequenceMatcher(None, w1, w2)
            criterias.append(s.find_longest_match(0,len(w1),0,len(w2)).size)
        else:
            criterias = [-1, -1, -1]
            criterias.append(-1)
            criterias.append(-1)        
        
        #TODO ajouter d'autres critères
        criterias.append(1 if l1["macroarea"] == l2["macroarea"] else 0)
        criterias.append(1 if l1["genus"] == l2["genus"] else 0)
        criterias.append(1 if l1["family"] == l2["family"] else 0)
        if(l1["latitude"] != None and l1["longitude"] != None and l2["latitude"] != None and l2["longitude"] != None):
            criterias.append(sqrt((l2["latitude"]- l1["latitude"])**2 + (l2["longitude"] - l1["longitude"])**2))
        else:
            criterias.append(-1)
                                      
        X_train_list.append(criterias)
        
    return X_train_list, y_train_list

lang_info = dict()
def find_language(name):
    if name in lang_info:
        return lang_info[name]
    
    names = name.split("_")
    if(len(names)>1):
        for nsplit in names:
            if not languages.loc[languages['name'].str.lower() == nsplit.lower()].empty:
                lang_info[name] = languages.loc[languages['name'].str.lower() == nsplit.lower()].iloc[0]
                return languages.loc[languages['name'].str.lower() == nsplit.lower()].iloc[0]
    else:
        if not languages.loc[languages['name'].str.lower() == name.lower()].empty:
            lang_info[name] = languages.loc[languages['name'].str.lower() == name.lower()].iloc[0]
            return languages.loc[languages['name'].str.lower() == name.lower()].iloc[0]
    raise ValueError('Language not found')
        


# In[ ]:


###
# ENTRAINEMENT
###
### DECOMMENTER POUR CHARGER UNE FOIS LES DONNEES
X_train_list, y_train_list = load_data() #assez long, executer une fois pour avoir les données


# In[ ]:


print(X_train_list[20])
train = pd.read_csv("./IE2012-lexical-data.train.csv")
print(find_language(train["language1"][20]))
print(find_language(train["language2"][20]))
#Algo de prédiction Ridge
X_train = np.array(X_train_list)
y_train = np.array(y_train_list)
poly3 = PolynomialFeatures(degree=3)
X_train_p3 = poly3.fit_transform(X_train)
kf = KFold(n_splits=5, shuffle=True)

print("Ridge")
min_alpha = None
best_alpha = None
alphas = [-1000, -100, -15,  -5]
models_to_plot = {-1000:231,-100:232, -15:233, -5:234}

# Initialisation d'un dataframe pour stocker les resultats des estimations
col = ['train mse','intercept'] + [i for i in criterias]
ind = ['model_alpha_%f'%i for i in alphas]
coef_matrix_simple_ridge = pd.DataFrame(index=ind, columns=col)
x=0
for i in alphas:
    mses = list()
    r2 = list()
    for train_index, test_index in kf.split(X_train):     
        X_train_sub, X_test_sub = X_train[train_index], X_train[test_index]
        Y_train_sub, Y_test_sub = y_train[train_index], y_train[test_index] 
                
        ridge = RidgeClassifier(fit_intercept=True, normalize=True, alpha=pow(10, i))  
        ridge.fit(X_train_sub, Y_train_sub)
        
#        y_train_hat= ridge.predict(X_train_sub)
        y_test_hat = ridge.predict(X_test_sub)
        
#        print("RID Train MSE:\t",np.mean((y_train_hat - Y_train_sub)**2))
#        print("RID Test MSE:\t", np.mean((y_test_hat - Y_test_sub)**2) )
        r2.append(r2_score(Y_test_sub, y_test_hat))
        mse = mean_squared_error(Y_test_sub, y_test_hat)
        mses.append(mse)
        
    average_alpha = sum(mses) / len(mses)
    average_r2 = sum(r2) / len(r2)
    print("Average test MSE for alpha =", i, " :\t", average_alpha, " R2=",average_r2)
    if min_alpha == None or average_alpha < min_alpha :
        min_alpha = average_alpha
        best_alpha = i
        best_r2 = average_r2
        
    ridge = RidgeClassifier(fit_intercept=True, normalize=True, alpha=pow(10, i))  
    ridge.fit(X_train, y_train)
    y_pred = ridge.predict(X_train)
        
    #resultat en format: MSE, intersection, coef
    mse = 1./X_train.shape[0]*sum((y_pred-y_train)**2)
    ret = [mse]    
    ret.extend(ridge.intercept_)    
    ret.extend(ridge.coef_[0])
    coef_matrix_simple_ridge.iloc[x,0:10] = ret
    x=x+1
        
print("Best alpha = ", best_alpha)
print("Average test MSE for alpha =", best_alpha, " :\t", min_alpha, " R2=",best_r2, "\n")
ridge = RidgeClassifier(fit_intercept=True, normalize=True, alpha=pow(10, best_alpha))  
ridge.fit(X_train, y_train)


# In[ ]:


#tableau des différents coef sur les différents alpha
pd.options.display.float_format = '{:,.2g}'.format
coef_matrix_simple_ridge


# In[ ]:


#Algo de prédiction Perceptron


##
# Perceptron simple
##

print("Perceptron")
mses = list()
r2 = list()
for train_index, test_index in kf.split(X_train):    
    X_train_sub, X_test_sub = X_train[train_index], X_train[test_index]
    Y_train_sub, Y_test_sub = y_train[train_index], y_train[test_index] 
    
    p = Perceptron(max_iter = 50)
    p.fit(X_train_sub, Y_train_sub)
    
#    Y_train_hat = p.predict(X_train_sub)    
    y_test_hat = p.predict(X_test_sub)    
    
#    print("P Train MSE:\t", np.mean((Y_train_hat-Y_train_sub)**2) )
#    print("P Test MSE:\t", np.mean((y_test_hat-Y_test_sub)**2) )
    print(".", end='')
    mse = np.mean((y_test_hat - Y_test_sub)**2)
    mses.append(mse)
    r2.append(r2_score(Y_test_sub, y_test_hat))
average_alpha = sum(mses) / len(mses)
average_r2 = sum(r2) / len(r2)
print("\nAverage test MSE for P :", average_alpha, " R2=",average_r2)

print("\n")


print("Perceptron P3")
##
# Perceptron avec données transformées
##
mses = list()
r2 = list()
for train_index, test_index in kf.split(X_train_p3):    
    X_train_sub, X_test_sub = X_train_p3[train_index], X_train_p3[test_index]
    Y_train_sub, Y_test_sub = y_train[train_index], y_train[test_index] 
        
    p = Perceptron(max_iter = 50)
    p.fit(X_train_sub, Y_train_sub)
    
#    Y_train_p3_hat = p.predict(X_train_p3)
    Y_test_p3_hat = p.predict(X_test_sub)    
    
#    print("P3 Train MSE:\t", np.mean((Y_train_p3_hat-Y_train_sub)**2) )
#    print("P3 Test MSE p3:\t", np.mean((Y_test_p3_hat-Y_test_sub)**2) ,)
    mse = np.mean((Y_test_p3_hat - Y_test_sub)**2)
    mses.append(mse)
    r2.append(r2_score(Y_test_sub, Y_test_p3_hat))
average_alpha = sum(mses) / len(mses)
average_r2 = sum(r2) / len(r2)
print("\nAverage test MSE for P3 :", average_alpha, " R2=",average_r2)
    
print("\n")
    


# In[ ]:


###
# PREDICTION DONNEES DE TEST
####

print("PREDICTION")
print("Lecture des données")
test = pd.read_csv("./IE2012-lexical-data.test.csv")

print("Récupération des données de test")

print("Algo prediction")
f = open("result.txt", "w")
f.write("Id,Category\n")
for i in tqdm.tqdm(range(len(test["word1"]))):
    Id = test["Id"][i]
    w1 = test["word1"][i]
    w2 = test["word2"][i]
    l1 = find_language(test["language1"][i])
    l2 = find_language(test["language2"][i])
    
    if(type(w1) == str and type(w2) == str and w1 != '' and w2 != ''):
        criterias = phonetic_score(w1, w2)
        #Taille des mots
        criterias.append(abs(len(w1)-len(w2)))
        #Prefix
        s = difflib.SequenceMatcher(None, w1, w2)
        criterias.append(s.find_longest_match(0,len(w1),0,len(w2)).size)
    else:
        criterias = [-1, -1, -1]
        criterias.append(-1)
        criterias.append(-1)  
        
        
    #TODO ajouter d'autres critères
    criterias.append(l1["macroarea"] == l2["macroarea"])
    criterias.append(l1["genus"] == l2["genus"])
    criterias.append(l1["family"] == l2["family"])
    if(l1["latitude"] != '' and l1["longitude"] != '' and l2["latitude"] != '' and l2["longitude"] != ''):
        criterias.append(sqrt((l2["latitude"]- l1["latitude"])**2 + (l2["longitude"] - l1["longitude"])**2))
    else:
        criterias.append(-1)
    
    
#    X_p3 = poly3.fit_transform(np.array(criterias).reshape(1, -1))
    
#    f.write(str(Id)+","+ str(p.predict(np.array(X).reshape(1, -1))[0]) + "\n")
    f.write(str(Id)+","+ str(ridge.predict(X_p3)[0]) + "\n")
    #f.write(str(i)+",0\n")
    

f.close()
print("Fin")

