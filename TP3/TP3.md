# TP3 FAA
# Régression linéaire

## 1
Calculer les estimateurs de la régression linéaire à une variable <br/>
y = bx + a<br/>
w = [a,b]<br/>
Donc on cherche à minimiser :<br/>
a, b = argmin(A,B) Si=1>N (yi - Bxi-A)²<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
^ ŷi=Bxi+A


## 2
0) Télécharger le jeu de données : <br/>
https://archive.ics.uci.edu/ml/machine-learning-databases/housing/<br/>

1) Charger le jeu de données sous la forme d'un dataframe _pandas_ (utiliser la méthode *read_table*)
et ajouter les colonnes associées aux noms des variables<br/>
Visualiser le dataframe : df.head(), df.describe()<br/>

2) Séparer les données associées aux variables explicatives (les "x") en les stockant dans une matrice X
et celles associées à la variable à expliquer (les "y", "MEDV") dans un vecteur y<br/>

Séparer X et y en 2 parties<br/>
X_train , X_test<br/>
Y_train , Y_test<br/>
Utiliser : sklearn.model_selection.train_and_test_split(X,y,test_size=0.3,random_state=5)<br/>

3) Implémenter la régression linéaire<br/>

4) Estimer les paramètres du modèle sur (X_train, Y_train) et évaluer les prédictions du modèle sur (X_test, Y_test)<br/>

5) Visualiser les paramètres et essayer de les interpréter<br/>

6) Répéter 4) en utilisant l'implémentation de la régression linéaire de sklearn<br/>
