import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split 

#Q1
df = pd.read_table('housing.data', sep='\s+', header=None, 
                      names=['CRIM','ZN','INDUS','CHAS','NOX','RM','AGE','DIS','RAD','TAX','PTRATIO','B','LSTAT','MEDV'])

#Q2
X = df.as_matrix(columns=df.columns[0:13])
y = df['MEDV'].values

X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.3,random_state=5)

#Q3
mul_X = X.transpose().dot(X)
inv = np.linalg.inv(mul_X)

mul_X_y = X.transpose().dot(y)

w = inv.dot(mul_X_y)
print("w =",w)

#Q4