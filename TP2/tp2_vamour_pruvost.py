import numpy as np
import matplotlib.pyplot as plt
import time

N = 100

# 1 - Création d'un fonction cible et ensemble de données D en 2D
# a - Choisir une droite aléatoire dans le plan comme fonction cible
# d'un côté de la droite, les Xi soient associés à une étiquette +1
# de l'autre côté de la droite, les Xi soient associés à une étiquette -1

# Création des 2 points de la droite
a, b, c, d = np.random.uniform(-1, 1, 4)

# Fonction de la droite
def droite(x):
    return (c-a)*x+(d-b)

# Vectorisation de la fonction
y = np.vectorize(droite)

xs = np.linspace(-2, 2, 5)
ys = y(xs)

plt.plot(xs, ys, '-')

# c -Générer un jeu de données aléatoirement dans le plan D = {(xi, yi)} avec N = 20

# Création de l'ensemble des points
points = np.random.uniform(-1, 1, N*2).reshape((N, 2))
plt.plot(points[:,0], points[:,1], '.')

# On calcule pour chaque point sa position par rapport à la fonction de départ
def sign(x, y):
    if(droite(x) >= y):
        return -1
    else:
        return 1

signs = np.vectorize(sign)

D = signs(points[:,0], points[:,1])

# Affichage du graphe
plt.plot(xs, ys, 'g-')
plt.plot(points[:,0], points[:,1], '.')
plt.show()

# 2 Implémenter le perceptron
# On défini la droite de départ
w0, w1, w2 = 0, 1, 0.5
def droiteh(x):
    return -(w1/w2)*x-w0


def signh(x, y):
    if(droiteh(x) >= y):
        return -1
    else:
        return 1

allOk = False
cpt = 0
µ = 0.2
# Tant que notre hypothèse ne convient pas on boucle
while not allOk :
    cpt = cpt + 1
    allOk = True
    # On boucle pour chaque point
    for i, point in enumerate(points) :        
        # Si le point est du mauvais côté de la droite, on ajuste la droite
        if signh(point[0],point[1]) != D[i]:
            plt.plot(point[0],point[1], '*')
            allOk = False
            
            # correction
            w0 = w0 + D[i]*µ
            w1 = w1 + µ*D[i]*point[0]
            w2 = w2 + µ*D[i]*point[1]
            
            # On affiche la modification, l'hypothèse est en bleu
            yh = np.vectorize(droiteh)
            ysh = yh(xs)
#            time.sleep(0.1)
            plt.plot(xs, ys, 'g-')
            plt.plot(points[:,0], points[:,1], '.')
            plt.plot(xs, ysh, 'b-')
            plt.show()
    # On rétréci le facteur d'apprentissage pour être de plus en plus précis
    µ = max(µ / 2, 0.01)
    
# 3 - Trouver le perceptron sur D - Combien d'itérations sont requises pour bien classifier D ?
print(cpt," itérations")

# 4 - Répéter avec 100$
# Changer N en haut du fichier

# Séparer D en 2 sous-ensembles 

print("Y =\n",D)

signsh = np.vectorize(signh)
Dh = signsh(points[:,0], points[:,1])
print("YH =\n",Dh)